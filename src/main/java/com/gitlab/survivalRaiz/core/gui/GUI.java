package com.gitlab.survivalRaiz.core.gui;

import dev.arantes.inventorymenulib.buttons.ClickAction;
import dev.arantes.inventorymenulib.menus.InventoryGUI;

import java.util.Map;

public abstract class GUI {
    private final Map<String, ClickAction> itemActionMap;
    private final GUIConf conf;

    public GUI(Map<String, ClickAction> itemActionMap, GUIConf conf) {
        this.itemActionMap = itemActionMap;
        this.conf = conf;
    }

    /**
     * Builds the {@link InventoryGUI} from the json data.
     * @return the {@link InventoryGUI} corresponding to the data.
     */
    public abstract InventoryGUI get();

    public Map<String, ClickAction> getItemActionMap() {
        return itemActionMap;
    }

    public GUIConf getConf() {
        return conf;
    }
}
