package com.gitlab.survivalRaiz.core.gui.scoreboard;

import com.gitlab.survivalRaiz.core.Core;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;

public class SRScoreboard {

    private final Core plugin;

    public SRScoreboard(Core plugin) {
        this.plugin = plugin;
    }

    /**
     * Gets the scoreboard
     * @param p The player tat will recieve
     * @param sName The name of the scoreboard to be built
     * @throws NullPointerException when the scoreboard is null
     */
    public void createScoreBoard(Player p, String sName) throws NullPointerException{
        final Scoreboard sb = Bukkit.getScoreboardManager().getNewScoreboard();

//        final Objective objective = sb.registerNewObjective("SB Unique Name", <"dummy">, "title");
        final Objective objective = sb.registerNewObjective("Main", "dummy", ChatColor.GOLD+"Survival Raiz");

        objective.setDisplaySlot(DisplaySlot.SIDEBAR);

        final Score ln = objective.getScore(
                ChatColor.DARK_GREEN + "Online: " + ChatColor.GOLD + plugin.getServer().getOnlinePlayers().size()
                        + ChatColor.YELLOW +  "/" + plugin.getServer().getMaxPlayers());
        ln.setScore(4);

        final Score ln1 = objective.getScore(
                ChatColor.DARK_GREEN + "Dinheiro: " + ChatColor.GOLD + plugin.getDbManager().getBalance(p.getUniqueId(),
                        resultSet -> {
                            try {
                                return Collections.singletonList(String.format("%.2f", resultSet.getDouble(1)) + "$");
                            } catch (SQLException throwables) {
                                throwables.printStackTrace();
                            } catch (NullPointerException e) {
                                return Collections.singletonList("0$");
                            }

                            return Collections.singletonList("0$");
                        }).get(0));
        ln1.setScore(3);

        final DateTimeFormatter f = DateTimeFormatter.ofPattern(ChatColor.DARK_GREEN + "d-MM-yyyy " + ChatColor.GOLD + " HH:mm");
        final Score ln2 = objective.getScore(LocalDateTime.now().format(f));
        ln2.setScore(2);

        final Score ln3 = objective.getScore(ChatColor.GOLD+"discord.com/invite/Esz8z8Rdd8");
        ln3.setScore(1);

        p.setScoreboard(sb);
    }
}
