package com.gitlab.survivalRaiz.core.gui.scoreboard;

import com.gitlab.survivalRaiz.core.Core;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class ScoreboardListener implements Listener {

    private final Core plugin;

    public ScoreboardListener(Core plugin) {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        this.plugin = plugin;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        load(e.getPlayer());
    }

    public void load(Player p) {
        new SRScoreboard(plugin).createScoreBoard(p, "Survival Raiz");
    }
}
