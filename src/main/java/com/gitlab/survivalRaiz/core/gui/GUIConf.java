package com.gitlab.survivalRaiz.core.gui;

import api.skwead.storage.file.json.JSONConfig;
import org.bukkit.plugin.java.JavaPlugin;

public class GUIConf extends JSONConfig<GUIData> {
    public GUIConf(JavaPlugin plugin, String menuName) {
        super(plugin.getDataFolder().getAbsolutePath()+"/gui/"+menuName+".json", GUIData.class);
    }
}
