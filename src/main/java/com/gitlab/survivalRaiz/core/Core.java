package com.gitlab.survivalRaiz.core;

import com.gitlab.survivalRaiz.core.db.DBConfig;
import com.gitlab.survivalRaiz.core.db.DBManager;
import com.gitlab.survivalRaiz.core.gui.scoreboard.ScoreboardListener;
import com.gitlab.survivalRaiz.core.messages.Message;
import com.gitlab.survivalRaiz.core.messages.MsgHandler;
import dev.arantes.inventorymenulib.listeners.InventoryListener;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Core extends JavaPlugin {

    private final MsgHandler msgHandler = new MsgHandler(this);
    private final DBManager dbManager = new DBManager(new DBConfig(this));

    private ScoreboardListener scoreboardListener = null;

    @Override
    public void onEnable() {
        final Set<CommandSender> console = new HashSet<>(Collections.singleton(getServer().getConsoleSender()));

        this.msgHandler.message(console, Message.DB_SETUP);
        this.dbManager.generate();
        this.msgHandler.message(console, Message.DB_READY);

        this.msgHandler.message(console, Message.GUI_SETUP);
        new InventoryListener(this);
        this.msgHandler.message(console, Message.GUI_READY);

//        new ScoreboardListener(this);
        scoreboardListener = new ScoreboardListener(this);

        this.msgHandler.message(console, Message.SERVER_ON);
    }

    /**
     * This method is a workaround for a faster implementation of a scoreboard.
     * This should be made in a much better way as soon as possible.
     * If you are reading this, please remind me of my poor life choices.
     * @param p the player whose scoreboard is to be updated.
     */
    @Deprecated
    public void reloadScoreboard(Player p) {
        int taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(this,
                () -> scoreboardListener.load(p), 0L, 60 * 20);

        if (!p.isOnline()) Bukkit.getScheduler().cancelTask(taskID);
    }

    @Override
    public void onDisable() {
        this.msgHandler.message(new HashSet<>(Collections.singleton(getServer().getConsoleSender())), Message.SERVER_OFF);
    }

    public MsgHandler getMsgHandler() {
        return msgHandler;
    }

    public DBManager getDbManager() {
        return dbManager;
    }
}
