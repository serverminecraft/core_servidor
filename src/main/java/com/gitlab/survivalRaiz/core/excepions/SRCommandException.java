package com.gitlab.survivalRaiz.core.excepions;

import api.skwead.exceptions.exceptions.CommandException;
import com.gitlab.survivalRaiz.core.Core;
import com.gitlab.survivalRaiz.core.messages.Message;
import org.bukkit.command.CommandSender;

public class SRCommandException extends CommandException {

    final Message message;
    final Core core;

    public SRCommandException(CommandSender troubleMaker, Message message, Core core) {
        super(troubleMaker, null, null);
        this.message = message;
        this.core = core;
    }

    public SRCommandException(CommandSender troubleMaker, Message message, String syntax, Core core) {
        super(troubleMaker, null, syntax);
        this.message = message;
        this.core = core;
    }

    @Override
    public void showError() {
        core.getMsgHandler().message(getTroubleMaker(), message);

        if (super.getSyntax() != null)
            core.getMsgHandler().message(getTroubleMaker(), Message.SYNTAX,
                    s -> s.replaceAll("%syntax%", getSyntax()));
    }
}
