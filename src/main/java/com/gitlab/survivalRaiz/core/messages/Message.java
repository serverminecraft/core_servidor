package com.gitlab.survivalRaiz.core.messages;

/**
 * List of all possible messages on the server
 */
public enum Message {
    /**
     * When the server is online.
     */
    SERVER_ON,
    /**
     * When the server is offline
     */
    SERVER_OFF,
    /**
     * When all the tasks prior to db
     */
    DB_SETUP,
    /**
     * When the database setup is finished
     */
    DB_READY,
    /**
     * When the GUI modularizer is getting ready
     */
    GUI_SETUP,
    /**
     * When the GUI modularizer is functional
     */
    GUI_READY,
    /**
     * Shows how much money said player has
     */
    PLAYER_BALANCE,
    /**
     * To show the player has no clan
     */
    PLAYER_HAS_NO_CLAN,
    /**
     * To show the rank the player has
     */
    PLAYER_CLAN_RANK,
    /**
     * When the clan has no base
     */
    CLAN_HAS_NO_BASE,
    /**
     * The invite was sent
     */
    CLAN_INVITE_SENT,
    /**
     * A player just invited you to their clan
     */
    CLAN_INVITATION,
    /**
     * A player just invited you to be their aly
     */
    CLAN_INV_ALY,
    /**
     * A player just invited you to be their rival
     */
    CLAN_INV_RIV,
    /**
     * You accepted the invite
     */
    CLAN_INVITE_ACCEPTED,
    /**
     * To display the clan's rank
     */
    CLAN_RANK,
    /**
     * The header for the clan top message
     */
    CLAN_TOP_HEADER,
    /**
     * Every line corresponding to a clan in the clan top message
     */
    CLAN_TOP_LINE,
    /**
     * The header for the alies list
     */
    CLAN_ALIES_HEADER,
    /**
     * Every line for the alies message
     */
    CLAN_LIST_ALIES,
    /**
     * The header for the alies list
     */
    CLAN_RIVALS_HEADER,
    /**
     * Every line for the rivals message
     */
    CLAN_LIST_RIVALS,
    /**
     * When someone invites a new player for the clan but it is full
     */
    CLAN_IS_FULL,
    /**
     * When the console tries to run a command that is only meant for players
     */
    PLAYER_ONLY_COMMAND,
    /**
     * Shows the command syntax
     */
    SYNTAX,
    /**
     * When a player tries to claim an already claimed chunk
     */
    CHUNK_ALREADY_CLAIMED,
    /**
     * When the player has not enough money to complete a task
     */
    PLAYER_BROKE,
    /**
     * When a terrain does not exist
     */
    TERRAIN_DOES_NOT_EXIST,
    /**
     * When the player tries to access a terrain he doesn't own
     */
    TERRAIN_IS_NOT_YOURS,
    /**
     * When a player's name or UUID is not found, making the task impossible
     */
    PLAYER_NOT_FOUND,
    /**
     * When an action cannot be completed because the terrain's owner did not pay his taxes
     */
    TERRAIN_TAX_FRAUD,
    /**
     * When the next tax needs to be paid before executing a certain action
     */
    PAY_NEXT_TAX,
    /**
     * Header for the list of terrains that can be bought by the public
     */
    AUCTION_TERRAIN_LIST_HEADER,
    /**
     * Every line of the public auctioned terrains
     */
    AUCTION_TERRAIN_LIST_ELEMENT,
    /**
     * When a kit cannot yet be claimed or does not exist
     */
    KIT_UNAVAILABLE,
    /**
     * When an invite cannot be found
     */
    INVITE_NOT_FOUND,
    /**
     * When a clan does not exist
     */
    NONEXISTENT_CLAN,
    /**
     * When a player needs to have a clan
     */
    PLAYER_NEEDS_CLAN,
    /**
     * When the command executer does not have the required permissions
     */
    NO_PERMISSIONS,
    /**
     * When the tag given to a clan does not have exactly 3 UTF-8 characters
     */
    INVALID_CLAN_TAG,
    /**
     * When a clan with the given name already exists. Used when a clan is being created
     */
    CLAN_ALREADY_EXISTS,
    /**
     * When a requested chat channel does not exist
     */
    NONEXISTENT_CHANNEL,
    /**
     * When the player tries to create a shop but is not looking at a chest
     */
    LOOK_AT_CHEST,
    /**
     * When the player tries to create a shop but the chest he's looking at does not contain only the item of the shop
     */
    CHEST_WITH_MULTIPLE_ITEMS,

    /**
     * When a Player joins the server
     */
    LOGIN,
    /**
     * When a request already exists
     */
    REQUEST_ALREADY_EXISTS,
    /**
     * When a request (membership, aliance or rivalry) has been sent
     */
    REQUEST_SENT,
}
