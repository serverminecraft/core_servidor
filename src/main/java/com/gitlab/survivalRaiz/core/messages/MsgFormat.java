package com.gitlab.survivalRaiz.core.messages;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Lists all possible message types.
 */
public enum MsgFormat {
    /**
     * For a normal server message.
     */
    MESSAGE("message"),
    /**
     * For a more visible message, intended to get more attention from the users.
     */
    SHOUT("shout"),
    /**
     * Message to show information.
     */
    INFO("info"),
    /**
     * Message to show a warning.
     */
    WARNING("warning"),
    /**
     * Message to show an error.
     */
    ERROR("error"),
    /**
     * Message tpo show everything went well.
     */
    SUCCESS("success");

    final String type;

    MsgFormat(String type) {
        this.type = type;
    }

    /**
     * @return All possible message types as String.
     */
    public static Set<String> getAllTypes() {
        final MsgFormat[] allFormats = MsgFormat.values();
        final String[] res = new String[allFormats.length];

        for (int i = 0; i < res.length; res[i] = allFormats[i++].getType());

        return new HashSet<>(Arrays.asList(res));
    }

    public String getType() {
        return type;
    }
}
