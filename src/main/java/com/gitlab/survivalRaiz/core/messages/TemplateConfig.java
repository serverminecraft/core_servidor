package com.gitlab.survivalRaiz.core.messages;

import api.skwead.storage.file.yml.YMLConfig;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.Set;

public class TemplateConfig extends YMLConfig {
    public TemplateConfig(String name, JavaPlugin plugin) {
        super(name, plugin);
    }

    @Override
    public void createConfig(Set<String> set) throws IOException {
        for (String msgFormat : set) {
            if (super.getConfig().get(msgFormat) == null) {
                super.getConfig().createSection(msgFormat);
                super.getConfig().set(msgFormat, "%msg%");
                super.getConfig().save(super.getFile());
            }
        }
    }
}
